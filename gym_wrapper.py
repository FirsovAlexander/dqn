import gym
import torch
import pyvirtualdisplay
from numpy import float32

__all__ = ["Gym"]

def CartPoleHandler(env):
	pic = env.render(mode='rgb_array')
	assert pic.shape == (400, 600, 3)

	height = pic.shape[0]
	width = pic.shape[1]
	half_window = 32

	scale = width/(2*env.x_threshold) # pixel cols in one game axis unit
	caret_center = int(env.state[0] * scale + width/2)

	caret_left_edge = 0
	caret_right_edge = 0

	if caret_center + half_window > width:
		caret_right_edge = width
		caret_left_edge = width - 2*half_window
	elif caret_center - half_window < 0:
		caret_left_edge = 0
		caret_right_edge = 2*half_window
	else:
		caret_left_edge = caret_center - half_window
		caret_right_edge = caret_center + half_window


	# caret_left_edge = 0
	# caret_right_edge = 0
	# if (env.state[0] - 0.25) < -env.x_threshold:
	# 	caret_left_edge = -env.x_threshold * scale + width/2
	# 	caret_right_edge = (0.5 - env.x_threshold) * scale + width/2
	# elif (env.state[0] + 0.25) > env.x_threshold:
	# 	caret_left_edge = (env.x_threshold - 0.5) * scale + width/2
	# 	caret_right_edge = env.x_threshold * scale + width/2
	# else:
	# 	caret_left_edge = (env.state[0] - 0.25) * scale + width/2
	# 	caret_right_edge = (env.state[0] + 0.25) * scale + width/2

	# caret_left_edge = int(caret_left_edge)
	# caret_right_edge = int(caret_right_edge)

	pic = pic[165:315, caret_left_edge:caret_right_edge]
	pic = torch.from_numpy(pic.astype(float32)).permute(2, 0, 1) / 255.
	assert (pic.shape == torch.Size([3, 150, 64]))

	return pic

handlers = {"CartPole-v0":CartPoleHandler, }

class Gym:
	_display = None

	def __init__(self, game):
		self._create_display()

		self.env = gym.make(game)
		self.env.reset()
		self.handler = handlers[game]

		self.start_state = self.process_state_representation(self.env)

	#todo
	def action_result(self, movement):
		prev_state = self.start_state
		_, reward, done, _ = self.env.step(movement)
		next_state = self.process_state_representation(self.env)
		self.start_state = next_state
		if done:
			self.reset()

		return (prev_state, next_state, movement, done, reward)

	def reset(self):
		self.env.reset()
		self.start_state = self.process_state_representation(self.env)

	def process_state_representation(self, env):
		return self.handler(env)

	def num_moves(self):
		return self.env.action_space.n

	def current_state(self):
		return self.start_state

	@classmethod
	def environments(cls):
		from gym import envs
		return envs.registry.all()

	@classmethod
	def _create_display(cls):
		'''creates virtual display for gym to be able to render a scene and return numpy arrays'''
		if cls._display is not None:
			cls._display.stop()
			cls._display = None
		cls._display = pyvirtualdisplay.Display(visible=False,  # use False with Xvfb
                                    size=(1400, 900))
		_ = cls._display.start()
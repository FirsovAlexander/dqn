import torch
from random import sample

class ReplayMemory:
	def __init__(self, capacity):
		self.capacity = capacity
		self.capacitor = []
		self.position = 0

	def store(self, element):
		if len(self.capacitor) < self.capacity:
			self.capacitor.append(None)
		self.capacitor[self.position] = element
		self.position = (self.position + 1) % self.capacity

	def batch(self, size):
		return sample(self.capacitor, size)

	# def multistep_batch(self, b_size, length):
	# 	assert length > 1

	# 	current_states = []
	# 	final_states = []
	# 	done = torch.zeros([length-1, b_size], dtype=torch.float32)
	# 	by_step_reward = []#torch.zeros([length, b_size], dtype=torch.float32)

	# 	indexes = sample(range(self.capacity), b_size)
	# 	reward = []
	# 	for i, ind in enumerate(indexes):
	# 		current_states.append(self.capacitor[ind][0].unsqueeze(0))
	# 		reward.append(self.capacitor[ind][4])
	# 	current_states = torch.cat(current_states, dim=0)
	# 	by_step_reward.append(torch.tensor(reward).unsqueeze(0))

	# 	for i in range(length-1):
	# 		reward = []
	# 		for j in range(size(indexes)):
	# 			indexes[j] = (indexes[j] + 1) % self.capacity
	# 			reward.append(self.capacitor[indexes[j]][4])
	# 			done[i][j] = self.capacitor[indexes[j]][3]
	# 		by_step_reward.append(torch.tensor(reward).unsqueeze(0))

	# 	final_states = 

	# def multistep_batch(self, b_size, length):
	# 	indexes = sample(range(self.capacity), b_size)
	# 	start_state = torch.cat([self.capacitor[ind][0].unsqueeze(0) for ind in indexes], dim=0)
	# 	done = torch.zeros([length+1, b_size])
	# 	reward = torch.zeros([length+1, b_size])
	# 	move = torch.tensor([self.capacitor[ind][1].unsqueeze(0) for ind in indexes], dtype=torch.long)

	# 	for i in range(length+1):
	# 		for j in range(b_size):
	# 			indexes[j] = (indexes[j] + 1) % self.capacity
	# 			done[i][j] = self.capacitor[indexes[j]][3]
	# 			reward[i][j] = self.capacitor[indexes[j]][3]

	# 	final = torch.cat([self.capacitor[ind][0].unsqueeze(0) for ind in indexes], dim=0)

	def multistep_batch(self, b_size, length):
		moves = []#torch.zeros(b_size, dtype=torch.long)#1xbsize
		not_done = [torch.ones(b_size, dtype=torch.float32)]#torch.zeros([length+1, b_size], dtype=torch.int32)#[torch.zeros(b_size)]#length+1 x bsize
		rewards = []#torch.zeros([length, b_size], dtype=torch.float32)#length X bsize
		current_state = []
		end_state = []

		indexes = sample(range(len(self.capacitor)), b_size)
		for i in range(length):
			not_done_el = []
			reward_el = []
			for j in range(len(indexes)):
				prev_state, next_state, movement, done, reward = self.capacitor[indexes[j]]
				if i == 0:
					moves.append(movement)
					current_state.append(prev_state.unsqueeze(0))
				if i == length-1:
					end_state.append(next_state.unsqueeze(0))
				not_done_el.append(int(not done))#not_done[1][i] = int(not done)
				reward_el.append(reward)#reward[0][i] = reward

				indexes[j] = (indexes[j]+1)%len(self.capacitor)
			not_done.append(torch.tensor(not_done_el, dtype=torch.float32))
			rewards.append(torch.tensor(reward_el, dtype=torch.float32))

		return torch.cat(current_state, dim=0), torch.cat(end_state, dim=0), rewards, not_done, torch.tensor(moves, dtype=torch.long)

		# indexes = sample(range(self.capacity), b_size)
		# not_done_el = []
		# reward_el = []
		# for i, ind in enumerate(indexes):
		# 	_, _, movement, done, reward = self.capacitor[ind]
		# 	not_done_el.append(int(not done))#not_done[1][i] = int(not done)
		# 	reward_el.append(reward)#reward[0][i] = reward
		# 	moves.append(movement)
		# moves = torch.tensor(moves, dtype=torch.long)
		# not_done.append(not_done_el)
		# reward.append(reward_el)

		# for j in range(length-2):
		# 	not_done_el = []
		# 	reward_el = []
		# 	not_done[j+1][i] = int(not done)
		# 	reward[j][i] = reward
import torch
from torch.nn import BatchNorm2d as bn
from torch.nn import Conv2d as conv
from torch.nn import Linear
from torch.nn.parameter import Parameter

import torch.nn.functional as F

class NoisyLinear(torch.nn.Module):
	def __init__(self, in_channels, out_channels, bias=True, mean=0., std=1.):
		super(NoisyLinear, self).__init__()

		self.weights = Parameter(torch.zeros([out_channels, in_channels], requires_grad=True))
		self.sigma_weights = Parameter(torch.normal(mean, std, (out_channels, in_channels), requires_grad=True))
		self.epsilon_weights = Parameter(torch.normal(mean, std, (out_channels, in_channels), requires_grad=False))

		self.bias = None
		if bias is True:
			self.bias = Parameter(torch.zeros(out_channels, requires_grad=True))
			self.sigma_bias = Parameter(torch.normal(mean, std, (out_channels,), requires_grad=True))
			self.epsilon_bias = Parameter(torch.normal(mean, std, (out_channels,), requires_grad=False))

	def forward(self, x):
		A = self.weights + self.sigma_weights * self.epsilon_weights
		bias = self.bias
		if bias is not None:
			bias = bias + self.sigma_bias * self.epsilon_bias
		return F.linear(x, A, bias)

class Player(torch.nn.Module):
	def __init__(self, num_actions, noisy=False):
		super(Player, self).__init__()
		self.num_actions = num_actions

		self.conv1 = conv(3, 32, kernel_size=5, stride=2)
		self.conv2 = conv(32, 32, kernel_size=5, stride=2)
		self.conv3 = conv(32, 32, kernel_size=5, stride=2)
		self.conv4 = conv(32, 32, kernel_size=5)

		self.bn1 = bn(32)
		self.bn2 = bn(32)
		self.bn3 = bn(32)
		self.bn4 = bn(32)

		self.linear = NoisyLinear(32*12, self.num_actions) if noisy else Linear(32*12, self.num_actions)

	def forward(self, x):
		x = F.relu(self.bn1(self.conv1(x)))
		x = F.relu(self.bn2(self.conv2(x)))
		x = F.relu(self.bn3(self.conv3(x)))
		x = F.relu(self.bn4(self.conv4(x)))

		x = x.reshape(-1, 32*12)
		return self.linear(x)

	def predict_reward(self, input):
		self.eval()
		out = None
		with torch.no_grad():
			out = self.forward(input)
		return out

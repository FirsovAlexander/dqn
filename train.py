import torch
import math
import argparse

from random import random, choice

from torch.optim import RMSprop
from torch.nn import SmoothL1Loss

from gym_wrapper import Gym
from model import Player
from replay_memory import ReplayMemory

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from IPython import display

from torch.utils.tensorboard import SummaryWriter

WIN_REDUCE = 0.999
NUM_EPOCHES = 1000000
EXPERT_UPDATE = 10
MAX_EPSILON = 0.9
MIN_EPSILON = 0.05
BATCH_SIZE = 128
REWARD = 1.
ACTIONS = 2

parser = argparse.ArgumentParser()
parser.add_argument("--steps", type=int, default=1)
parser.add_argument("--noisy", type=bool, default=False)
args = parser.parse_args()
steps = args.steps
noisy = args.noisy

writer = SummaryWriter("./graphic")
trainer = Gym("CartPole-v0")
model = Player(ACTIONS, noisy=noisy)
expert = Player(ACTIONS, noisy=noisy)
expert.load_state_dict(model.state_dict())
expert.eval()

memory = ReplayMemory(40000)
eps = 0 if noisy else MAX_EPSILON
optimizer = RMSprop(model.parameters())
criterion = SmoothL1Loss()

best_win = 0

def choose_action(state, player, eps):
	if random() < eps:
		return choice(range(ACTIONS))
	else:
		action = None
		player.eval()
		with torch.no_grad():
			action = player.forward(state.unsqueeze(0)).argmax(dim=1)[0].item()
		player.train()
		return action

def fill_memory(memory, player, trainer, n_steps, eps):
	i = 0
	while True:
		i += 1
		action = choose_action(trainer.current_state(), player, eps)
		result = trainer.action_result(action)
		memory.store(result)
		if i >= n_steps and result[-2] is True:
			break

def reduce_eps(max_eps, min_eps, n):
	eps = min_eps + (max_eps - min_eps) * math.exp(-n/400)
	return eps

def optimize(optimizer, criterion, pred, true):
	optimizer.zero_grad()
	loss = criterion(pred, true)
	loss.backward()
	optimizer.step()
	return loss.item()

def eval_model(model, trainer, n_starts):
	seq_durations = torch.zeros(n_starts, dtype=torch.float32)
	cartoon = [[]]*n_starts

	max_ind = 0
	#print("Evaluating")
	for i in range(n_starts):
		#print(f"Restart {i+1} of {n_starts}")
		trainer.reset()
		while True:
			cartoon[i].append(trainer.current_state())
			action = choose_action(trainer.current_state().cuda(), model, 0.)
			result = trainer.action_result(action)
			seq_durations[i] += result[-1]
			if result[-2] is True:#done
				break
		if seq_durations[i] > seq_durations[max_ind]:
			max_ind = i
	#play_sequence(cartoon[max_ind])
	return int(seq_durations.mean().item()), seq_durations.std().item()

def play_sequence(seq):
	seg = list(map(lambda x: x.permute(1, 2, 0), seq))
	fig = plt.figure()

	ims = []
	for _ in range(10):
		a = torch.rand([15, 15, 3]).numpy()
		im = plt.imshow(a, animated=True)
		ims.append([im])

	ani = animation.ArtistAnimation(fig, ims, interval=300, repeat=False)
	#plt.show()
	display.clear_output(wait=True)
	display.display(plt.gcf())

fill_memory(memory, model, trainer, 20000, eps)#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
model = model.cuda()
#expert = expert.cuda()

for i in range(NUM_EPOCHES):
	loss = 0
	#mean_duration = 0
	length = 0
	win = 0
	while True:
		win += REWARD * (WIN_REDUCE ** length)
		length += 1

		action = choose_action(trainer.current_state().cuda(), model, eps)
		result = trainer.action_result(action)
		memory.store(result)

		current_state, end_state, reward, not_done, moves = memory.multistep_batch(BATCH_SIZE, steps)
		prediction_win = model(current_state.cuda())[torch.arange(BATCH_SIZE), moves].unsqueeze(1)
		reward.append(expert.predict_reward(end_state).amax(dim=1))

		#reward.append(expert.predict_reward(end_state).amax(dim=1))
		reduce_coeff = 1.#torch.ones(BATCH_SIZE, dtype=torch.float32)
		expected_optimal_win = torch.zeros(BATCH_SIZE, dtype=torch.float32)

		for r, nd in zip(reward, not_done):
			reduce_coeff = reduce_coeff * nd
			expected_optimal_win = expected_optimal_win + reduce_coeff * r
			reduce_coeff *= WIN_REDUCE

		expected_optimal_win = expected_optimal_win.unsqueeze(1).cuda()

		# batch = memory.batch(BATCH_SIZE)
		# current_state = torch.cat(list(map(lambda x: x[0].unsqueeze(0), batch)), dim=0).cuda()
		# non_fail = torch.tensor([i for i in range(len(batch)) if batch[i][3] is False], dtype=torch.long)
		# next_state = torch.cat(list(map(lambda x: batch[x][1].unsqueeze(0), non_fail)), dim=0).cuda()
		# actions = torch.tensor([item[2] for item in batch], dtype=torch.long)

		# prediction_win = model(current_state)[torch.arange(BATCH_SIZE), actions].unsqueeze(1)
		# real_win = torch.ones(BATCH_SIZE, dtype=torch.float32).cuda()#first score counted
		# real_win[non_fail] += expert.predict_reward(next_state).amax(dim=1) * WIN_REDUCE
		# real_win.unsqueeze_(1)

		loss += optimize(optimizer, criterion, prediction_win, expected_optimal_win)
		if not noisy:
			eps = reduce_eps(MAX_EPSILON, MIN_EPSILON, i)

		if result[-2] is True:#done
			loss /= length
			# writer.add_scalar("win_against_session", win, i+1)
			break

	#length = sum(game)/len(game)
	print("Epoch:{}\tLoss:{}\tDuration:{}".format(i+1, loss, length))
	#eps = reduce_eps(MAX_EPSILON, MIN_EPSILON, i)

	if (i+1)%EXPERT_UPDATE == 0:
		print("Updated expert")
		expert.load_state_dict(model.state_dict())#????????CUDA!?
		expert.eval()
		expert = expert.cpu()

	if (i+1)%100 is 0:
		mean_win, dispersion = eval_model(model, trainer, 10)
		writer.add_scalar("win_against_session_multistep_noisy", mean_win, (i+1)//100)
		if mean_win > best_win: #and dispersion < lowest_dispersion:
			best_win = mean_win

			win_str = "{:.3f}".format(mean_win)
			disp_str = "{:.3f}".format(dispersion)
			win_str = win_str.replace(".", "p")
			disp_str = disp_str.replace(".", "p")

			torch.save(model.state_dict(), "./model_{}_{}.pt".format(win_str, disp_str))
		
